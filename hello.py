#!/usr/bin/env python3
"""
A simple hello world program.
"""
from ev3dev.ev3 import Sound, LargeMotor


Sound.speak('Hello world! May the force be with you!').wait()

right_wheel = LargeMotor('outB')
left_wheel = LargeMotor('outC')

assert right_wheel.connected, 'Engine not attached: right wheel'
assert left_wheel.connected, 'Engine not attached: left wheel'

right_wheel.run_timed(speed_sp=600, time_sp=1500)
left_wheel.run_timed(speed_sp=600, time_sp=1500)
